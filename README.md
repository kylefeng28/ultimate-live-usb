# Ultimate Live USB
Files and guides to create bootable systems from ISOs *without erasing the entire freaking drive*.

## Windows

With existing Windows:
https://www.drivereasy.com/knowledge/burn-windows-10-iso-usb/

Without existing Windows:

FAT32, as the UEFI standard only supports FAT32.

Download ISO from
https://www.microsoft.com/en-us/software-download/windows10ISO

Mount the ISO file (it contains a UDF file system), and then copy everything except `sources`.
```
autorun.inf
boot
bootmgr
bootmgr.efi
efi
setup.exe
support
```
Then, copy everything inside `sources`, except `install.wim`. This file is too large to fit on a FAT32 system, as the limit is 4GB, so we need to split it.

On Windows:
```
Dism /Split-Image /ImageFile:install.wim /SWMFile:install.swm /FileSize:2048
```

On Linux, install [libwim](https://wimlib.net/), then:
```
wimsplit install.wim install.swm 2048
```
